/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	     entry* vc_cache_blocks,
        	                 	 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         bool debug)
{

  
   int tag= (int)l1_vc_info-> l1_tag;
   int idx= (int)l1_vc_info-> l1_idx;
   int associativity=(int)l1_vc_info-> l1_assoc;
  // int victim_associativity=(int)l1_vc_info-> vc_assoc;
   int victim_associativity = 16;
   bool l1_hit = false;
   bool vc_hit = false;
   int rpvaluehit;
   int vc_hitpos;
   int pos_hit;
   int vc_pos_hit;


   /* Check hit in L1 */
  	for (size_t i = 0; i < associativity; i++)
	{
		if (l1_cache_blocks[i].tag == tag && l1_hit == false && l1_cache_blocks[i].valid)
		{
			l1_hit = true;

	   		if (loadstore){
		   		l1_cache_blocks[i].dirty = true;
		   		l1_result-> miss_hit = HIT_STORE;
	   		}

	   		else {
		   		l1_result->miss_hit = HIT_LOAD;
	   		}	

	   		/*Apply LRU*/
	   		for (size_t j = 0; j < associativity; j++){
		   		if(l1_cache_blocks[j].rp_value > l1_cache_blocks[i].rp_value){
			   		l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value - 1;
		   		}
			}

		   	l1_cache_blocks[i].rp_value = associativity - 1;
		}
	}
 
    
    /* Check hit in VC */
  	for (size_t i = 0; i < victim_associativity; i++)
	{
		if (vc_cache_blocks[i].tag  ==  tag && vc_hit == false && vc_cache_blocks[i].valid)
		{
			vc_hit = true;
			vc_pos_hit = i;
		}
	}


   /*Miss in L1*/

   if (!l1_hit){
	   size_t pos_lru = 0;
   	   for (size_t j = 0; j < associativity; j++){ 
		   if(l1_cache_blocks[j].rp_value == 0){
			  pos_lru = j;
		   }
		}

		if (l1_cache_blocks [pos_lru].valid){
			l1_result->dirty_eviction = l1_cache_blocks[pos_lru].dirty;
			l1_result->evicted_address = l1_cache_blocks[pos_lru].tag;
		}

		l1_cache_blocks[pos_lru].valid = 1;

		/*Check the Victim Cache*/

		/*When is a hit on VC*/

		if (vc_hit){
			/*Switch  L1 y VC*/
			l1_cache_blocks[pos_lru].valid = vc_cache_blocks[vc_pos_hit].valid;
            l1_cache_blocks[pos_lru].dirty = vc_cache_blocks[vc_pos_hit].dirty;
			l1_cache_blocks[pos_lru].tag = vc_cache_blocks[vc_pos_hit].tag;


			/*Put the last result on the first position of the VC*/
            vc_cache_blocks[0].valid = 1;
            vc_cache_blocks[0].dirty = l1_result->dirty_eviction;
            vc_cache_blocks[0].tag = l1_result->evicted_address;


            if(loadstore){
				vc_result->miss_hit = HIT_STORE;
			}
			else{
				vc_result->miss_hit = HIT_LOAD;
			}	
		}

		/*Miss on VC*/
		if (!vc_hit){
            /*Save result*/
			if (loadstore){
				l1_cache_blocks [pos_lru].dirty = true;
				l1_result->miss_hit = MISS_STORE;
			}

			else{
				l1_result->miss_hit = MISS_LOAD;
			}

			/*When is a miss, we need FIFO logic*/
			if (vc_cache_blocks [victim_associativity - 1].valid){
				/*Last One in the VC is out if the VC is full*/
            	vc_result->dirty_eviction = vc_cache_blocks[victim_associativity-1].dirty;
				vc_result->evicted_address = vc_cache_blocks[associativity-1].tag;				
			}

         	for(size_t j = victim_associativity -1; j > 0; j--){
            	vc_cache_blocks[j].tag = vc_cache_blocks[j-1].tag;
            	vc_cache_blocks[j].dirty = vc_cache_blocks[j-1].dirty;
            	vc_cache_blocks[j].valid = vc_cache_blocks[j-1].valid;
         	}
			 
			vc_cache_blocks[0].tag = l1_result->evicted_address;
         	vc_cache_blocks[0].dirty = l1_result->dirty_eviction;
         	vc_cache_blocks[0].valid = 1;

			if(loadstore){
				vc_result->miss_hit = MISS_STORE;
			}
			else{
				vc_result->miss_hit = MISS_LOAD;
			}	

			l1_cache_blocks[pos_lru].tag = l1_vc_info->l1_tag;

		}

      /*Apply LRU*/
      for(size_t j = 0; j < associativity; j++){
         if(l1_cache_blocks[j].rp_value > l1_cache_blocks[pos_lru].rp_value) {
		 l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value-1;
		 }
      }
      l1_cache_blocks[pos_lru].rp_value = associativity-1;
      
   }
   return OK;
}
