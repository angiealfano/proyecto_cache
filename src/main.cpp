#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <L1cache.h>
#include <debug_utilities.h>
// Las limpio luego - Luis
#include <string>
#include <fstream>
#include <sstream>
#include <cstring>
#include <stddef.h>
////
#include <netinet/in.h>
#include <math.h>
using namespace std;



/* Helper funtions */


#define KB 1024
#define ADDRSIZE 32
using namespace std;





void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}


  // printf("Do something :), don't forget to keep track of execution time");

  


  /* Print Statistics */
void print_statistics(int size, int associativity, int block_size, int rp,  float load_hit, float load_miss, float store_hit, float store_miss, int dirty_evic) {
    float total_miss = load_miss + store_miss;
    float total_hit = load_hit + store_hit;
    float missrate = (float)total_miss/(float)(total_hit+total_miss);
    cout << "_____________________________________" << endl;
    cout << "Cache parameters:"<< endl;
    cout<<"_______________________________________" << endl;  
    cout << "Cache Size (KB):               "<< size << endl;
    cout << "Cache Associativity:           "<< associativity << endl;
    cout << "Cache Block Size (bytes):      "<< block_size << endl;
    cout << "_______________________________________" << endl;
    cout << "Simulation results:"<< endl;
    cout <<"_______________________________________" << endl;
    cout << "CPU time (cycles):             " << "" << endl; /*BUSCAR ESTOS DOS*/
    cout << "AMAT(cycles):                  "<< "" << endl;
    cout << "Overall miss rate::            "<< total_miss/(total_hit + total_miss) << endl;
    cout << "Read miss rate:                "<< load_miss/(total_hit + total_miss) << endl;
    cout << "Dirty evictions:               "<< dirty_evic << endl;
    cout << "Load misses:                   "<< load_miss << endl;
    cout << "Store misses:                  "<< store_miss << endl;
    cout << "Total misses:                  "<< total_miss << endl;
    cout << "Load hits:                     "<< load_hit << endl;
    cout << "Store hits:                    "<< store_hit << endl;
    cout << "Total hits:                    "<< total_hit << endl;
    cout << "_______________________________________" << endl;

 
} 
  


int main(int argc, char * argv []) {

  /* Parse argruments */
  int counter = 1;
  int size, associativity, block_size, rp;
    while(counter < argc){ // Se obtienen los parámetros brindados por el usuario 
        char* param = argv[counter];
        if(strcmp(param, "-t") == 0){
            size = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-l") == 0){
            block_size = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-a") == 0){
            associativity = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-rp") == 0){
            rp = stoi(argv[counter+1]);
        }

        counter++;
    }

  /* Idx, tag y offset */
  struct cache_params cache_params1 = {size,associativity,block_size};
  struct cache_params cache_params2 = {size*4,associativity*2,block_size}; // The #2 variables are for the L2 cache
  struct cache_field_size field_size1;
  field_size_get(cache_params1, &field_size1); 
   


  /*Cache*/
  int sets = (size*KB)/(block_size*associativity);
  int sets2 = (size*4*KB)/(block_size*associativity*2);
  struct entry cacheblock[sets][associativity];
  struct entry cacheblock2[sets2][associativity*2];
  /*Inicial condition for cache*/
  for(int i = 0; i < sets; i++) {
    for (int j = 0; j < associativity; j++) {
      cacheblock[i][j].valid = false;
      cacheblock[i][j].dirty = false;
      cacheblock[i][j].tag = 0;
     if (rp==0){
        cacheblock[i][j].rp_value = j; /*NO ESTOY SEGURA*/
     }
     if (rp==1){
        cacheblock[i][j].rp_value = 1;
     }
     if (rp==2){
        cacheblock[i][j].rp_value = 3;
     }
    }
  }

  /*Operation result*/
  struct operation_result result;

  /* Get trace's lines and start your simulation */
  int load_store;
  long address;
  int marker; 
  int idx, tag, ic;
  int instruction_count; 
  int debug = 0;
  int contadorPrueba = 1;
  float load_hit = 0;
  float store_hit = 0;
  float load_miss = 0;
  float store_miss = 0;
  int dirty_evic = 0;
  string trace;
  
  //while(scanf("%c %d %lx %d\n", &marker,  &load_store, &address, &instruction_count) != EOF){
    while (getline(cin,trace)){
    ic = stoi(trace.substr(13));  /*Corto el string y con el stoi lo obtengo en la base que necesito*/
    address = stoi (trace.substr(4,8), nullptr, 16);
    load_store = stoi (trace.substr(2), nullptr, 2);

    address_tag_idx_get(address,field_size1,&idx,&tag);

    switch (rp)
    {
      /*cambiar nru, lru, srrip*/
    case 0:
      lru_replacement_policy(idx, tag, associativity, load_store, cacheblock[idx], &result, debug);
      break;
    case 1: 
      nru_replacement_policy(idx, tag, associativity, load_store, cacheblock[idx], &result, debug);
      break;
    case 2: 
      srrip_replacement_policy(idx, tag, associativity, load_store, cacheblock[idx], &result, debug);
      break;
    }
    //cout << "Tag: " << result[0] << "idx: " << result[1] << "offset: " << result[2] << "\n";
    
  

      /* Values */
    if (result.miss_hit == HIT_LOAD){
    load_hit++;
    } 
    else if (result.miss_hit == MISS_LOAD){
      load_miss++;
    }
    else if (result.miss_hit == HIT_STORE){
      store_hit++;
    }
    else if (result.miss_hit == MISS_STORE){
      store_miss++;
    }
    if (result.dirty_eviction == true){
      dirty_evic++;
    }
  }
  
  print_statistics(size, associativity, block_size, rp,  load_hit, load_miss,  store_hit,  store_miss, dirty_evic);
  
return 0;
}
