/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int l1_l2_entry_info_get (const struct cache_params l1_params, const struct cache_params l2_params, long address, entry_info* l1_l2_info, bool debug){
	struct cache_field_size field_size1;
	struct cache_field_size field_size2;

	field_size_get(l1_params, &field_size2);
	field_size_get(l2_params, &field_size1); 

	l1_l2_info->l1_assoc = l1_params.asociativity;
	l1_l2_info->l1_idx = field_size1.idx;
	l1_l2_info->l1_offset = field_size1.offset;
	l1_l2_info->l1_tag = field_size1.tag;

	l1_l2_info->l2_assoc = l2_params.asociativity;
	l1_l2_info->l2_idx = field_size2.idx;
	l1_l2_info->l2_offset = field_size2.offset;
	l1_l2_info->l2_tag = field_size2.tag;
	return OK;

}

int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{


    if(l1_l2_info->l1_idx < 0 || l1_l2_info->l1_tag < 0 || l1_l2_info->l1_assoc <= 0 || l1_l2_info->l2_idx < 0 || l1_l2_info->l2_tag < 0 || l1_l2_info->l2_assoc <= 0) {
   return PARAM;
   }

	bool hit_l1 = false;
	bool hit_l2 = false;

	for (int i = 0; i < l1_l2_info->l1_assoc; i++){
	/*Searching hit in L1*/
		if((l1_cache_blocks[i].tag == l1_l2_info->l1_tag) && l1_cache_blocks[i].valid)
		{
			hit_l1 = true;
		
         l1_result->dirty_eviction = false;

			for(int j = 0; j < l1_l2_info->l1_assoc; j++){
				if ((l1_cache_blocks[j].rp_value > l1_cache_blocks[i].rp_value)&&(l2_cache_blocks[j].rp_value > 0)){
					l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
				}
			}
			/* MRU is the hit block */
			l1_cache_blocks[i].rp_value = l1_l2_info->l1_assoc-1;

			/*Searching hit in L2*/
			for(int k = 0; k < l1_l2_info->l2_assoc; k++){
				if(l1_l2_info->l2_tag == l2_cache_blocks[k].tag && l2_cache_blocks[k].valid){
               l2_result->dirty_eviction = false;
	            hit_l2 = true;
					for(int j = 0; j < l1_l2_info->l2_assoc; j++){
						if ((l2_cache_blocks[j].rp_value > l2_cache_blocks[k].rp_value)&&(l2_cache_blocks[j].rp_value > 0)){
							l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
						}
					}
					/* MRU is the hit block*/
					l2_cache_blocks[k].rp_value = l1_l2_info->l2_assoc-1;
			
					if (loadstore == 1) {
                  /*l2 dirty in the block of l1*/
                  l2_cache_blocks[i].dirty = true;
						l1_result->miss_hit = HIT_STORE;
						l2_result->miss_hit = HIT_STORE;
						
					}

					else{ 
						l1_result->miss_hit = HIT_LOAD;
						l2_result->miss_hit = HIT_LOAD;
					}

					break;
				}
			}
			break;
		}
	}

	/* L1 miss and L2 hit */
	if (!hit_l1){
		for (int i = 0; i < l1_l2_info->l2_assoc; i++){
			/* L2 hit */
			if (l1_l2_info->l2_tag == l2_cache_blocks[i].tag){
				hit_l2 = true;
            l2_result->dirty_eviction = false;
				for(int j = 0; j < l1_l2_info->l2_assoc; j++){
					if (l2_cache_blocks[j].rp_value > l2_cache_blocks[i].rp_value && (l2_cache_blocks[j].rp_value > 0)){
						l2_cache_blocks[j].rp_value = l1_l2_info->l2_assoc-1;	
					}
				}
				/* MRU is the hit block */
				l2_cache_blocks[i].rp_value = l1_l2_info->l2_assoc-1;
				if (loadstore == 1) {
					l2_cache_blocks[i].dirty = true;
					l1_result->miss_hit = MISS_STORE;
					l2_result->miss_hit = HIT_STORE;
				}
				else{ 
					l1_result->miss_hit = MISS_LOAD;
					l2_result->miss_hit = HIT_LOAD;
				}
				break;
			}
		}
	}

   /*L1 miss*/
	if (!hit_l1){
		for (int i = 0; i < l1_l2_info->l1_assoc; i++){
			if(l1_cache_blocks[i].rp_value == 0){
				for(int j = 0; j < l1_l2_info->l1_assoc; j++){
					if (l1_cache_blocks[j].rp_value > 0){
						l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value - 1;
					}
				}
				/* MRU is the new block*/
				l1_cache_blocks[i].rp_value = l1_l2_info->l1_assoc-1;

            /*Write evicted block*/
				l1_result->evicted_address = l1_cache_blocks[i].tag;
            
            /*Insert new block*/
				l1_cache_blocks[i].tag = l1_l2_info->l1_tag;
				l1_cache_blocks[i].valid = true;
				l1_result->dirty_eviction = false;
				if (loadstore) {
					l1_result->miss_hit = MISS_STORE;
				}
				else{
					l1_result->miss_hit = MISS_LOAD;
				}
				break;
			}
		}
	}

	/*L2 miss*/
	if (!hit_l2){
		/* Find LRU */
		for (int i = 0; i < l1_l2_info->l2_assoc; i++){
			if(l2_cache_blocks[i].rp_value == 0){
				for(int j = 0; j < l1_l2_info->l2_assoc; j++){
					if (l2_cache_blocks[j].rp_value > 0){
						l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value -1;
					}
				}
				// Write evicted block
				l2_result->evicted_address = l2_cache_blocks[i].tag;

            // Insert new block
				l2_cache_blocks[i].rp_value = l1_l2_info->l2_assoc-1;
				l2_cache_blocks[i].tag = l1_l2_info->l2_tag;
				l2_cache_blocks[i].valid = true;

				if (loadstore == 1){
               l2_cache_blocks[i].dirty = true;
					l2_result->miss_hit = MISS_STORE;
					
				}
				else {
               l2_cache_blocks[i].dirty = false;
					l2_result->miss_hit = MISS_LOAD;
				}
				break;
			}
		}
	}


	return OK;
}
