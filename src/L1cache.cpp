/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <bits/stdc++.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;



int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
  if(cache_params.size <= 0 || cache_params.asociativity <= 0 || cache_params.block_size <= 0){
      return PARAM;
   }
   int total_blocks= cache_params.size*KB/(cache_params.asociativity);
   int total_sets = total_blocks/cache_params.block_size;
   field_size->offset = log2(cache_params.block_size);
   field_size->idx = log2(total_sets);
   field_size->tag = ADDRSIZE - (field_size->idx + field_size->offset);

  return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{ 
   /*Idx value*/
   *idx = (address >> field_size.offset) & (long)((1<<field_size.idx)-1);
   /*Tag value*/
   *tag = address >> (field_size.offset + field_size.idx);
}


int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  if (idx<0 || tag<0 || associativity<1) {
    return PARAM;
  }
  //We define some values
  int index_rrip=0;
  int m;
  bool found = false;
  bool hit = false;
  int block_to_replace = false;
  //Logic of m value according to associativity
  if (associativity <= 2) {
    m=1;
  }
  if (associativity > 2) {
    m=2;
  }
  int farlong = pow(2,m)-2;
  int distant = pow(2,m)-1;

  //i means the way
  for (size_t i = 0; i < associativity; i++) {
    if (cache_blocks[i].valid && cache_blocks[i].tag == tag ) {
      //We have a hit
      cache_blocks[i].rp_value = 0; //Update RRPV bit to a 0
      result->dirty_eviction = false;
      hit = true;
      if (loadstore) {
        cache_blocks[i].dirty = true;
        result->miss_hit = HIT_STORE;
      }
      else{
        result->miss_hit = HIT_LOAD;
      }
      return OK;
    }
  }
  //Cache miss
  if (!hit) { 
    //Search for first '3' from left
    while (found != true) {
      for (size_t k = 0; k < associativity; k++) {
        if (cache_blocks[k].rp_value == distant && index_rrip == 0) {
          index_rrip =k;
          found = true;
          break;
        }
      }
      //3 not found, increment all RRPV and search again for 3
      if (found==false) {
        for (size_t k = 0; k < associativity; k++) {
          cache_blocks[k].rp_value = cache_blocks[k].rp_value+1;
        }
      }
    }
    //Store the evicted
    if (cache_blocks[index_rrip].valid) {
      result->dirty_eviction = cache_blocks[index_rrip].dirty;
      result->evicted_address = cache_blocks[index_rrip].tag;
    }
    //Block replacemente and RRPV value set
    cache_blocks[index_rrip].tag =tag;
    cache_blocks[index_rrip].rp_value = farlong;
    cache_blocks[index_rrip].valid = 1;

    if (loadstore) {
      cache_blocks[index_rrip].dirty =true;
      result->miss_hit = MISS_STORE;
    }
    else{
      cache_blocks[index_rrip].dirty =false;
      result->miss_hit = MISS_LOAD;
    }
    return OK;
  }

   return ERROR;
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

    if(idx < 0 || tag < 0 || associativity <= 0) {
   return PARAM;
   }

    bool hit = false;

   /* Now we check for a Hit */
   for(int i = 0; i < associativity; i++){
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid == 1){
         hit = true;
         result->dirty_eviction = false;
         if(loadstore){
            result->miss_hit = HIT_STORE;
            cache_blocks[i].dirty = true;
         
         }
         else{
            result->miss_hit = HIT_LOAD;
         }

         /* LRU process */
         for(int j = 0; j < associativity; j++){
            if(cache_blocks[j].rp_value > cache_blocks[i].rp_value){
               cache_blocks[j].rp_value =  cache_blocks[j].rp_value-1;
            }
         }

         /* The hit block is now MRU */
         cache_blocks[i].rp_value = associativity - 1; 
         return OK;
      }
   }

   /* If the hit wasn't found*/
   if(hit == false){

      /* Search for the least*/
      for(int i = 0; i < associativity; i++){
         if(cache_blocks[i].rp_value == 0){

            /* Saving evicted */
            if(cache_blocks[i].valid == true){
               result->dirty_eviction = cache_blocks[i].dirty;
               result->evicted_address = cache_blocks[i].tag;
               
            }

            /* Replacement */
            cache_blocks[i].tag = tag;
            cache_blocks[i].valid = true;
            

            if(loadstore){
               cache_blocks[i].dirty = true;
               result->miss_hit = MISS_STORE;
            }

            else{
               cache_blocks[i].dirty = false;
               result->miss_hit = MISS_LOAD;
            }

            /* LRU process */
            for(int j = 0; j < associativity; j++){
               if(cache_blocks[j].rp_value > cache_blocks[i].rp_value){
                  cache_blocks[j].rp_value =  cache_blocks[j].rp_value-1;
               }
            }
            
            /* The hit block is now MRU */
            cache_blocks[i].rp_value = associativity - 1; 

            return OK;
         }
      }
   }
   return OK;
}
int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{
    if(idx < 0 || tag < 0 || associativity <= 0) {
   return PARAM;
   }


   if(idx < 0 || tag < 0 || associativity <= 0) {
   return PARAM;
   }

   bool hit = false;
   int hit_pos = 0;
   bool first_one = false;
   bool ONE = true;

   /* Searching for a hit in the blocks */

   for (int i = 0; i < associativity; i++ ){
      if (cache_blocks[i].tag == tag && cache_blocks[i].valid == 1 && hit == false){
         hit = true;
         hit_pos = i;
      }
   }

   /*HIT*/

   if (hit){
      cache_blocks[hit_pos].rp_value=0;
      /*Checking Load or Store, True for store*/
      if (loadstore == true) {
         cache_blocks[hit_pos].dirty = true;
         operation_result->miss_hit = HIT_STORE;
      }
      else {
         operation_result->miss_hit = HIT_LOAD;
      }
    }
      /*When there is a hit, NO eviction*/
      operation_result-> dirty_eviction = false;
      
  
   
   
   /* MISS */
   if (hit == false){
      while (ONE) {
         for (int j = 0; j < associativity; j++){
            if (cache_blocks[j].rp_value == 1){
               cache_blocks[j].rp_value = 0;
               if (cache_blocks[j].valid){
                  operation_result->dirty_eviction = cache_blocks[j].dirty;
                  operation_result->evicted_address = cache_blocks[j].tag;
               }

               /*Eviction*/
               cache_blocks[j].tag = tag;
               cache_blocks[j].valid = 1;
               ONE = false; 

               if (loadstore == true) {
               cache_blocks[j].dirty = true;
               operation_result->miss_hit = MISS_STORE;
               }
               else {
               operation_result->miss_hit = MISS_LOAD;
               cache_blocks[j].dirty = false;
               }

            break;

            }
            else { 
               /*There is no ones on the rp.value*/
               ONE = true;
            }
         }

         if (ONE){
            /*Set all rp values = 1*/
            for (int k = 0; k < associativity; k++){
               cache_blocks[k].rp_value = 1;
            }
         }

         if(debug) print_way_info(idx,associativity,cache_blocks);



      }
   }
 

 
   return OK;
}
