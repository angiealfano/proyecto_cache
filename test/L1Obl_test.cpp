/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L1Obl.h>

using namespace std;

class OBLcache : public ::testing::Test{
  protected:
	int debug_on = 0;
	virtual void SetUp()
	{
	/* Parse for debug env variable */
	get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: miss on new block and prefetch 
 * Choose a random associativity
 * Force a miss on block A
 * Check miss_hit_status == MISS_X
 * Check obl_tag for block A is 0
 * Check block A + 1 is updated
 * Check dirty bit of block A + 1 is 0
 * Check block A + 1 obl_block is 1 
 */
TEST_F(OBLcache, miss_prefetch) {

}
 
/*
 * TEST2: hit on block with obl 0
 * Choose a random associativity
 * Fill a cache line cache_block with obl = 0
 * Fill a second cache line (cache_block_obl) with obl = 1
 * Force a hit on block A in cache_blocks
 * Check obl_tag for block A is 0
 * Check cache_block_obl remains the same
 */
TEST_F(OBLcache, hit_no_prefetch) {
 
}

/*
 * TEST3: hit on block with obl 1
 * Choose a random associativity
 * Fill a cache line cache_block with obl = 1
 * Fill a second cache line cache_block_obl
 * Force a hit on block A in cache_blocks
 * Check obl bit for block A get set to 0
 * Check block A + 1 is inserted in cache_block_obl
 * Check block A + 1 obl bit is set to 1
 */
TEST_F(OBLcache, hit_prefetch) {
  
}
  
