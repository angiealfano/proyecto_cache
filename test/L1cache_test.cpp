/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug_on = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){  /*PRIMERO SE HACE EL MISS LOAD CON i=0 */
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3; /*Preguntar por esta linea: 2^M-1*/
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);   /*Aca se revisan*/
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit); /*Resultado sea igual al esperado*/
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug_on = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4); /*Porque asi*/
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){  /*PRIMERO SE HACE EL MISS LOAD CON i=0 */
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i; /*Por ser LRU, va a depender de la via donde se encuentre*/
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);   /*Aca se revisan*/
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit); /*Resultado sea igual al esperado*/
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug_on = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4); /*Porque asi*/
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){  /*PRIMERO SE HACE EL MISS LOAD CON i=0 */
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1; /*Por ser NRU, puedo inicializar todas en 1*/
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);   /*Aca se revisan*/
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit); /*Resultado sea igual al esperado*/
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */


TEST_F(L1cache, promotion){
  int status;
  int i;
  int idx;
  int tag;
  int policy;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug_on = 0;
  struct operation_result result = {};

  policy = rand()%3;

  /* Choose a random associativity */
  associativity =  1 << (rand()%4);

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;

  struct entry cache_line[associativity];


  DEBUG(debug_on,Checking promotion);

    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;


      if(policy == 0){
        cache_line[i].rp_value = i;
      }
      else if(policy == 1){
        cache_line[i].rp_value = 1;
      }
      else if(policy == 2){
         if (associativity <= 2)
          {
            cache_line[i].rp_value = 1;
          }
          else
          {
            cache_line[i].rp_value = 3;
          }
      }
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }

      /* Force a hit on A */
    int tag_A= cache_line[0].tag;
    /*Insert a new block  (hit because A is in the Cache)*/


      if(policy == 0){
        lru_replacement_policy(idx,
                                     tag_A,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }
      else if(policy == 1){
        nru_replacement_policy(idx,
                                     tag_A,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }
      else if(policy == 2){
        srrip_replacement_policy(idx,
                                     tag_A,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }


    /*Checking rp for block A*/
    uint8_t rp_A= cache_line[0].rp_value;
    if (policy == 0){
      EXPECT_EQ(rp_A, (associativity-1));
    }
    else if (policy == 1){
      EXPECT_EQ(rp_A, 0);
    }
    else if (policy == 2){
      EXPECT_EQ(rp_A, 0);
    }


    /*Inserting new blocks until A es evicted (N)ways*/
    if (policy == 2 && associativity >2 ){
      while (result.evicted_address != tag_A) {
          tag = rand()%4096;
          srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }


    }
    else
    {
      for(int i = 0; i < associativity; i++){
       tag = rand()%4096;
       if(policy == 0){
        lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }
      else if(policy == 1){
        nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }
      else if(policy == 2){
        srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      }


      }
    }
      /*Checking Tag of evicted*/
      EXPECT_EQ(result.evicted_address, tag_A);
}

/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */


TEST_F(L1cache, writeback){
   int status;
  int i;
  int idx;
  int tag;
  int policy;
  int associativity;
  bool loadstore = 0;
  bool debug_on = 0;
  struct operation_result resultB = {};
  struct operation_result resultA = {};

  /* Choose a random policy */
  policy = rand()%3;


  /* Choose a random associativity */
  associativity = 1 << (rand()%4);

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line_A[associativity];
  /* Fill  with read operations A */
  loadstore = 0;
  DEBUG(debug_on,Checking writeback);

    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line_A[i].valid = true;
      cache_line_A[i].tag = rand()%4096;
      cache_line_A[i].dirty = 0;


      if(policy == 0){
        cache_line_A[i].rp_value = i;
      }
      else if(policy == 1){
        cache_line_A[i].rp_value = 1;
      }
      else if(policy == 2){
        cache_line_A[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }

      while (cache_line_A[i].tag == tag) {
        cache_line_A[i].tag = rand()%4096;
      }
    }

    int tag_A = tag;

      if (policy == 0){  
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
      }
      else if (policy == 1) {
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
      }
      else if (policy == 2) {
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
      }

    loadstore = 1;
   /*Force a write hit on A */
      if (policy == 0){  
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
      }
      else if (policy == 1) {
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
      }
      else if (policy == 2) {
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
      }


    /*Force a read hit on A */
    loadstore = 0;
      if (policy == 0){  
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
      }
      else if (policy == 1) {
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
      }
      else if (policy == 2) {
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
      }

  /*New block B */
  idx = rand()%1024;
  tag = rand()%4096;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line_B[associativity];
  loadstore = 0; /*Read*/

  DEBUG(debug_on,Checking writeback);

    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line_B[i].valid = true;
      cache_line_B[i].tag = rand()%4096;
      cache_line_B[i].dirty = 0;


      if(policy == 0){
        cache_line_B[i].rp_value = i;
      }
      else if(policy == 1){
        cache_line_B[i].rp_value = 1;
      }
      else if(policy == 2){
        cache_line_B[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }

      while (cache_line_B[i].tag == tag) {
        cache_line_B[i].tag = rand()%4096;
      }
    }

    /* Insert a new block B, force a read hit */
    int tag_B = tag;

      if (policy == 0){
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_B,
                                &resultB,
                                bool(debug_on));
      }
      else if (policy == 1){
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_B,
                                   &resultB,
                                   bool(debug_on));
      }
      else if (policy == 2){
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_B,
                                         &resultB,
                                         bool(debug_on));
      }
  

 /* New blocks until B is evicted*/

      while(resultB.evicted_address != tag_B)
      {
        tag = rand()%4096;
          if (policy == 0){     
            lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line_B,
                                    &resultB,
                                    bool(debug_on));
          }
          else if (policy == 1){
            nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line_B,
                                       &resultB,
                                       bool(debug_on));
          }
          else if (policy == 2){
            srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line_B,
                                             &resultB,
                                             bool(debug_on));

          }
      }


    DEBUG(debug_on,Checking dirty bit);


    EXPECT_EQ(resultB.dirty_eviction, 0);
    EXPECT_EQ(resultB.evicted_address, tag_B);

    /*New Blocks until A is evicted*/

      while(resultA.evicted_address != tag_A)
      {
        tag = rand()%4096;
        if (policy == 0){
            lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line_A,
                                    &resultA,
                                    bool(debug_on));
        }
        if (policy == 1){
            nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line_A,
                                       &resultA,
                                       bool(debug_on));
        }
        if (policy == 2){
            srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line_A,
                                             &resultA,
                                             bool(debug_on));
        }
      }
      /*Check dirty bit for block A*/
      EXPECT_EQ(resultA.dirty_eviction, 1);
      EXPECT_EQ(resultA.evicted_address, tag_A);
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  bool loadstore = 1;
  bool debug = 0;

  operation_result result = {};
  /*Random Policy*/
 // int policy = rand()%3;
  int policy = 1;
  /* Fill a random cache entry with invalid parameters*/
  idx = -1*rand()%1024;
  tag = -1*rand()%4096;
  associativity = -1*(1 << (rand()%4));
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  entry cache_line[-1 * associativity];

     /* Fill cache line */
    for ( i =  0; i <-1* associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      if(policy == 0){
        cache_line[i].rp_value = i;
      }
      else if(policy == 1){
        cache_line[i].rp_value = 1;
      }
      else if(policy == 2){
        cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3; 
      }

      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    if(policy==0){
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    }
    else if(policy==1){
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    }
    else if(policy==2){
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    }


    /*Check*/
    EXPECT_EQ(status, 1);  /*1 significa PARAM*/
}
